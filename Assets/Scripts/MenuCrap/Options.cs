﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    public AudioMixer audioMixer; 
    public Toggle twoPlayer;
    public void SetVolume(float volume)  // volume control
    {
        audioMixer.SetFloat("volume", volume); // attached to a slider to be used for music volume control
    }
    public void SetFullScreen(bool isFullScreen) // full screen or not
    {
        Screen.fullScreen = isFullScreen; // toggles the screen to full
    }
    public void SetPlayerAmount() // one or two player
    {
        if (!twoPlayer.isOn) // set player one
        {
            PlayerPrefs.SetInt("PlayerAmount", 0); // saves amount of player to player pref's
        }
        else // set player two
        {
            Debug.Log("2");
            PlayerPrefs.SetInt("PlayerAmount", 1); // saves amount of players to player pref's
        }
    }
}
