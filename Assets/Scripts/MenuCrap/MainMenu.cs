﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
   

    public void PlayGame() // loads the next level
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // gets the next level in the build order 
    }

    public void QuitGame() // quit game
    {
        Application.Quit(); // quits the game
    }

}
