﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankData : MonoBehaviour
{
    //displays variables for the designer to change
    [Header("Components")] //shows in the inspector sort of a label for designers to know what section and type these are
    public Transform tf;
    public TankMover mover;

    //displays vairables for the designer to change
    [Header("Variables")]//shows in the inspector sort of a label for designers to know what section and type these are

    public float MoveSpeed; // called in TankMover section world direction to make the player go foward
    public float ReverseMoveSpeed; //not currently called yet
    public float ShotsPerSecond; // not currretnly called yet
    public float RotateSpeed; // speed to rotate called in TankMover section direction to set a speed for turning 8 time.delta time for a smooth transition

    [Header("Health")]
    [HideInInspector]public float Health; // hide in the inspector
    public float MaxHealth; // max health for player or enemy
    public float GiveScore; // score to be added to the game manager
    public float Damage; // damage the bullet will do
    
    public int ScoreToAdd; // score to be added

    public AIController Color;
    
    public int Red;
    public int Green;
    public int Blue;
    public List<Renderer> ColoringTank = new List<Renderer>();

    public void RemovingHealth(float RemoveHealth) // remove damage from health amount
    {
        Health -= RemoveHealth;  // take away health
    }

    private void Awake()
    {
        tf = GetComponent<Transform>();//what compnet tf will pull from the object
        mover = GetComponent<TankMover>();//what mover will pull from the object
        Health = MaxHealth; // set health to max health
    }
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Health <= 0) // if helath is less than or equal to 0 add score and destory the tank player or enemy
        {
            GameManager.instance.AddScore(ScoreToAdd); // add score to the game manager
            Destroy(gameObject); // destory the game object when its health is below zero
            
        }


        if (Color != null) // if color is not null
        {

            if (Color.CurrentState == AIController.AIStates.Idle) // loop though waypoints counting up
            {
                for (int i = 0; i < ColoringTank.Count; i++) 
                {
                    ColoringTank[i].material.color = new Color(0, 0, 0); // color change
                }

            }
            else if (Color.LoopType == AIController.LoopTypes.Loop) // loop though waypoints counting up
            {
                for (int i = 0; i < ColoringTank.Count; i++)
                {
                    ColoringTank[i].material.color = new Color(255, 0, 0); // color change to red
                }

            }
            else if (Color.LoopType == AIController.LoopTypes.PingPong) // loop though waypoints counting up
            {
                for (int i = 0; i < ColoringTank.Count; i++)
                {
                    ColoringTank[i].material.color = new Color(0, 255, 0); // color change to red
                }

            }
            else if (Color.LoopType == AIController.LoopTypes.Random) // loop though waypoints counting up
            {
                for (int i = 0; i < ColoringTank.Count; i++)
                {
                    ColoringTank[i].material.color = new Color(0, 0, 255); // color change to red
                }

            }
            else if (Color.LoopType == AIController.LoopTypes.Turret) // loop though waypoints counting up
            {
                for (int i = 0; i < ColoringTank.Count; i++)
                {
                    ColoringTank[i].material.color = new Color(150, 0, 150); // color change to red
                }

            }
        }
           
    }
}
