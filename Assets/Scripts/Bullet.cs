﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    // bullet stuff
    public enum ShootingScheme { AI, SpaceBar } // two different types for shooting
    public ShootingScheme HowToShoot; // how it will shoot
    public GameObject bullet; // prefab for the object
    public float BulletSpeed = 10; // base speed for the bullet
    public float PlayShoot = 10; // how long until the player can shoot after pressing space bar
    public float AIShoot = 15; // how long until the AI can shoot again after firing
    public float CountUp = 0; // base count up 
    private TankData Data; // pull from tank data

    void Start()
    {
        Data = GetComponentInParent<TankData>();  // pull from the parent TANK DATA
    }
    void Update() // make sure it's in update so it keeps looping through
    {
        if (HowToShoot == ShootingScheme.SpaceBar) // if the boolian is set to 
        {
            CountUp += Time.deltaTime; // the count is added up to a point and then runs

            if (Input.GetKey(KeyCode.Space)) // when the space bar is hit
            {
                if (CountUp >= PlayShoot) // if the time is greater or equal to the playshoot, run
                {
                    GameObject instBullet = Instantiate(bullet, transform.position, transform.rotation) as GameObject; // make the game gameobject bullet prefab
                    Rigidbody instBulletRigidBody = instBullet.GetComponent<Rigidbody>(); // pull from the ridgid body

                    KillMe damage = instBullet.GetComponent<KillMe>(); // pull from kill me
                    damage.Damage = Data.Damage; // pull damage from kill me

                    instBulletRigidBody.AddForce(transform.forward * BulletSpeed); // speed of the bullet
                    CountUp = 0; // reset count up to zero
                }
            }
        }
        else if (HowToShoot == ShootingScheme.AI) // if boolian is set this no space bar is needed
        {
            CountUp += Time.deltaTime; // count up
            if (CountUp >= AIShoot) // if time is greater or equal to AISHOOT run
            {
                GameObject instBullet = Instantiate(bullet, transform.position, transform.rotation) as GameObject; // make the game object bullet
                Rigidbody instBulletRigidBody = instBullet.GetComponent<Rigidbody>(); // pull from ridgid body
                instBulletRigidBody.AddForce(transform.forward * BulletSpeed); // make the bullet go foward
                CountUp = 0; // count up is set to zero and it all runs again  
            }
        }
         
    }
}
