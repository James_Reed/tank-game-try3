﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(TankData))] //this script won't run unless the object also has the script tankdata on it
public class TankMover : MonoBehaviour
{

    public TankData data;  // Calls tank data script 
    private CharacterController characterController; //pulls from character controller on the obeject

    // Start is called before the first frame update
    void Start()
    {
        data = GetComponent<TankData>(); //fills in the variable data with components from tank data
        characterController = GetComponent<CharacterController>();// fills character controller variable with character contoller component
    }



    // Update is called once per frame
    void Update()
    {

    }


    public void Move(Vector3 worldDirectionToMove) // setting movment from location in the world
    {
        Vector3 directionToMove = data.tf.TransformDirection(worldDirectionToMove);
        characterController.SimpleMove(directionToMove * data.MoveSpeed);//Getting the current direction of the character in the world and passing it to the variable worldDirectionToMove. It is also multiplied by move speed set by the designer in the inspector
    }

    public void Rotate(float direction)
    {
        data.tf.Rotate(new Vector3(0, direction * data.RotateSpeed * Time.deltaTime, 0));// this is the rotate function and it uses time.deltatime to make it a smoother transition it is also mulitplied by rotate speed set by the designer in the inspector
    }

    public void RotateTowardsPlayer(Vector3 LookVector) // makes the AI tank rotate towards the player
    {
        Vector3 VectorToPlayer = LookVector; //setting vector 3 to look look vector

        Quaternion TargetQuartenion = Quaternion.LookRotation(VectorToPlayer, Vector3.up); // sets the x to VectorToPlayer and y vector to 1

        data.tf.rotation = Quaternion.RotateTowards(data.tf.rotation, TargetQuartenion, data.RotateSpeed * Time.deltaTime); // sets x to dat.tf.roation sets y to targetquaterion sets z to data.rotate speed and multiplied by 
    }

 
}
