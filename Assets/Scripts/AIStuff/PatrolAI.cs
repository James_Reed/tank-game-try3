﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolAI : AIController // calls from aicontroller
{
    public GameObject[] waypoint;
    // Start is called before the first frame update
    void Start()
    {
        waypoint = GameObject.FindGameObjectsWithTag("Waypoint");
        for (int i = 0; i < waypoint.Length; i++)
        {
            waypoints.Add(waypoint[i].transform);
        }
        CurrentWayPoint = 0; // starts current way point loop at 0 which starts the looping
    }

    // Update is called once per frame
    void Update()
    {
        switch (CurrentState)
        {
            case AIStates.Idle: // idle state
                Idle(); // idle fucntion called

                if (Time.time > StateStartTime + 3.0f) // count down timer till it goes into a patrol state
                {
                    ChangeState(AIStates.Patrol); // changes the state to patrol
                }
                break;

            case AIStates.Patrol: // sets state to patrol
                Patrol(); // calls patrol function on every update

                break;
        }
    } 


}
