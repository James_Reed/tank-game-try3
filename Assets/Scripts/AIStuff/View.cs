﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View : MonoBehaviour
{
    public AIController Ref;
    public float allowedDistance;
    public float vision = 45;

    void Start()
    {
        Ref = GetComponent<AIController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Ref.target != null) // if not target
        {
            Vector3 targetDir = Ref.target.transform.position - transform.position; // sets distance to player
            float z = targetDir.z; // pulls the axis z of target direction

            if (z <= allowedDistance) // if z is less than or equal to allowed distance
            {
                float angle = Vector3.Angle(targetDir, transform.forward); // float for position in general

                if (angle < vision) // if the angle is within the vision "Cone"
                {
                   Ref.SeekPoint(Ref.target.transform.position);
                }
            }
        }
    }

    void OnTriggerEnter(Collider other) // noise ball that was created using a sphere then taking away everyting but the collider picks up the object on the enter
    {
        if (other.gameObject.tag == "Noise") // check tag
        {
            Ref.target = other.gameObject; // target set to object that just collides
        }
    }

    void OnTriggerExit(Collider other) // noise ball that was created using a sphere then taking away everyting but the collider gets rid of the object on another trigger enter
    {
        if (other.gameObject.tag == "Noise") // check tag
        {
            Ref.target = null; // set target to null
        }
    }
}   