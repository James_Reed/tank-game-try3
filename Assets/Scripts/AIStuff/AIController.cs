﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    public List<Waypoints> WP = new List<Waypoints>();

    public TankData pawn; // calls from tank data to get values
    public enum LoopTypes //enum allows for more variable types
    {
        Loop, //loops points
        Stop,
        PingPong, // bounce between points 
        Random, // random points
        Turret

    };

    // coloring tnaks
    public int Red;
    public int Green;
    public int Blue;

    public int distance; // distnce to object
    public Transform lookPosition; // which way is it looking
    public GameObject target; // what's the targer


    public enum AIStates // Ai states for 
    {
        Idle, Patrol, Chase, Flee // states idel (not moving) patrol (goes to patrol points) chase (goes after player) flee (goes away from player)
    }

    public enum AIAvoidState // states for avoiding objects
    {
       None, TurnToAvoid, MoveToAvoid // avoid states none (doesn't avoid) turn to avoid (how long does it take to turn) move to avoid (how long it moves away)  
    }

    public LoopTypes LoopType; // loop type
    public List<Transform> waypoints; // listof transform waypoints
    public int CurrentWayPoint; // what the current way point is
    public float CutOff; // distance to pawn
    public bool IsFoward; // if the tank is going foward
    public float StateStartTime; // idle time
    public AIStates CurrentState; // what stae is it in


    // avoiding objects section
    public float FeelerDistance; // feeler distance
    public AIAvoidState CurrentAvoidState = AIAvoidState.None; // starting state
    public float AvoidMoveTime; // time to run avoid
    public float StartAvoidTime; // time to new state

    

    public void ChangeState(AIStates NewState) // state to start in
    {
        StateStartTime = Time.time; // time
        CurrentState = NewState; // starting state
    }

    public void ChangeAvoidState(AIAvoidState NewState) // avoidence state
    {
        StartAvoidTime = Time.time; // starting avoidence
        CurrentAvoidState = NewState; // change to new state
    }

    public void Idle() // has the tank do nothing
    {
        Red = 255;
        Blue = 255;
        Green = 255;
        // Idle will have the enemy do nothing
    }

    public bool IsBlocked() // sends out raycast in a set distance to see if the way is blocked then 
    {
        if (Physics.Raycast(pawn.tf.position, pawn.tf.forward, FeelerDistance))// lets the tank know that something is there
        {
            Debug.Log(FeelerDistance);
            return true; // if it is true the tank will try to turn away from what is blocking it
        }
        return false; // the tank will keep going foward
    }

    public void Seek(Transform target) // seek out player
    {
        switch (CurrentAvoidState) // current state
        {
            case AIAvoidState.None: // state of no avoidence
                Vector3 targetVector = (target.position- pawn.tf.position).normalized; //passes the pawn location to target point
                pawn.mover.RotateTowardsPlayer(targetVector); // rotates towards the player
                pawn.mover.Move(Vector3.forward); // moves the tank foward

                if (IsBlocked()) // blocked 
                {
                    ChangeAvoidState(AIAvoidState.TurnToAvoid); // turn state to avoide objects
                }
                break;
            case AIAvoidState.TurnToAvoid: // turn to avoide
                pawn.mover.Rotate(1); // rotate
                if (!IsBlocked()) //if blocked
                {
                    ChangeAvoidState(AIAvoidState.MoveToAvoid); // avoide by moving
                }
                break;
            case AIAvoidState.MoveToAvoid: // move
                pawn.mover.Move(Vector3.forward); // go foward
                if (IsBlocked())
                {
                    ChangeAvoidState(AIAvoidState.TurnToAvoid); // turn to avoide
                }
                if (Time.time > StartAvoidTime + AvoidMoveTime) // how long to run
                {
                    ChangeAvoidState(AIAvoidState.None); // change to none state
                }
                break;
                    
        }
    }

    public void SeekPoint(Vector3 targetPoint) // targets a point
    {
        Vector3 targetVector = (targetPoint - pawn.tf.position).normalized; //passes the pawn location to target point
        pawn.mover.RotateTowardsPlayer(targetVector); // rotates towards the player
        pawn.mover.Move(Vector3.forward); // moves the tank foward
    }

    public void Flee(Transform target) // RUN AWAY!!
    {
        Vector3 targetVector = (target.position - pawn.tf.position); // get pawn location and pass to enemy
        Vector3 awayVector = -targetVector; // sets awayvector to negative target player.... the other way 
        pawn.mover.RotateTowardsPlayer(awayVector); // rotates towards the new vector that is away from the player
        pawn.mover.Move(Vector3.forward); // move the player foward
    }

    public void Patrol() // patrol type functions
    {
        if (target == null)
        {
            Seek(waypoints[CurrentWayPoint]); // seek out next waypoint

            if (Vector3.Distance(pawn.tf.position, waypoints[CurrentWayPoint].position) <= CutOff)
            {
                if (IsFoward) // loop in order going up
                {
                    CurrentWayPoint++; // seek out next point
                }
                else
                {
                    CurrentWayPoint--; // seek out waypoint one less in order list
                }

                if (CurrentWayPoint >= waypoints.Count || CurrentWayPoint < 0)
                {
                    if (LoopType == LoopTypes.Loop) // loop though waypoints counting up
                    {
                        CurrentWayPoint = 0; // starts it at waypoint 0
                    }
                    else if (LoopType == LoopTypes.Random) // go to a random waypoint
                    {
                        CurrentWayPoint = Random.Range(0, waypoints.Count); // randomizes the waypoint order but keeps the same range
                    }
                    else if (LoopType == LoopTypes.PingPong) // bounce between 2 waypoints
                    {
                        IsFoward = !IsFoward; // if it is not foward then it goes to the previous point
                        if (CurrentWayPoint >= waypoints.Count) // if the next waypoint is grater than or equal to the current it goes to the previous one
                        {
                            CurrentWayPoint = waypoints.Count - 1; // select previous way point
                        }
                        else
                        {
                            CurrentWayPoint = 0; // waypoint is set to element "0"
                        }
                    }
                    else if (LoopType == LoopTypes.Turret)
                    {
                        pawn.MoveSpeed = 0; // doesn't move
                        pawn.ReverseMoveSpeed = 0; // doesn't move
                        pawn.RotateSpeed = 270; // rotation speed
                    }
                }
            }   
        }
        else
        {
            
            //Seek(target.transform);
        }
    }
}
