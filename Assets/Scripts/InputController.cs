﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public enum ControlScheme {WASD, ArrowKeys}; // enum allows for adding varaibles to variables to be used in if statments

    public TankData pawn; //Pawn is the overall thing while tankdata is its subsection of pawn
    public ControlScheme controlScheme; //variable controlScheme
   


    // Start is called before the first frame update
    void Start()
    {
    }

    // Move directions
    void Update()
    {
        
          
        //Simplified way of movement that uses AXIS instead of listing out each key to hit for up and down
        //Vector3 directionToMove = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")); 
        Vector3 directionToMove = Vector3.zero; // sets starting transforms to zero on frame update
        if (controlScheme == ControlScheme.WASD)
        {
            if (Input.GetKey(KeyCode.W)) // makes the tank go foward when W is pressed W (0,0,1)
            {
                directionToMove += Vector3.forward;
                pawn.mover.Move(directionToMove);
                Debug.Log("Pressed w go foward"); // make sure the button is pressed
            }

            if (Input.GetKey(KeyCode.S)) // makes the tank go "negative foward" when s is pressed (0,0,-1)
            {
                directionToMove += -Vector3.forward;
                pawn.mover.Move(directionToMove);
                //Debug.Log("Pressed s go backword"); // make sure the button is pressed
            }

            if (Input.GetKey(KeyCode.A)) // makes the tank rotate "Regative right" when a is pressed. It is multiplied by time.deltatime to make the tranition smoother
            {
                pawn.mover.Rotate(-pawn.RotateSpeed * Time.deltaTime);
                //Debug.Log("Pressed a rotate left"); // make sure the button is pressed
            }

            if (Input.GetKey(KeyCode.D)) // makes the tank rotate right when D is pressed. It is multiplied by time.deltatime to make the tranition smoother
            {
                pawn.mover.Rotate(pawn.RotateSpeed * Time.deltaTime);
                //Debug.Log("Pressed d rotate right"); // make sure the button is pressed
            }
        }

        //repeat of everything before but using arrow keys instead
        else if (controlScheme == ControlScheme.ArrowKeys) //This version of code allows for the code to toggleded between in the inspector
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                directionToMove += Vector3.forward;
                pawn.mover.Move(directionToMove);
                Debug.Log("Pressed Up Arrow go foward");
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                directionToMove += -Vector3.forward;
                pawn.mover.Move(directionToMove);
                Debug.Log("Pressed Down Arrow go back");
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                pawn.mover.Rotate(-pawn.RotateSpeed * Time.deltaTime);
                Debug.Log("Pressed Left Arrow rotate left");
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                pawn.mover.Rotate(pawn.RotateSpeed * Time.deltaTime);
                Debug.Log("Pressed Right Arrow rotate right");
            }
        }
        //this is the final step after either s or w is pressed it moves the tank to the new positio

        ////pawn.mover.Move(directionToMove);
    }
}
