﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
	public enum RandomOrDay { Day, Random }; // random or day room creation
	[Header("Random map or Map of the day")] // header
	public RandomOrDay SeedType; // seed type day or random
	public int SeedMapBool; // bool to turn it
	public string Date; // current date
	public string OldDate; // old date

    public Respawn[] respawn; // respawn locations
    public GameObject player; // one player
    public GameObject playerTwo; // second player

	[Header("Score")]
	public int Score; // score

    public int spawn; // spawn location
    public int spawnTwo; // spawn location for player 2
    public bool isPickup; // is it a pickup
    public bool twoPlayers; // two players


    //public EnemyRespawn[] EnemyTankRespawn;

    public static GameManager instance; // create and setting game manager
	public List<InputController> players; // listing out player and controllers


	public void AddScore(int PlayerScore) // add score int so the points will be a whole number
	{
		Score += PlayerScore; // add to player score
	}


	// Start is called before the first frame update
	void Awake()
    {
        if (instance == null)
        {
            instance = this; // this object
            DontDestroyOnLoad(gameObject); // gameobject doesn't get destroyed when loading into a new scene or restarting the current one
        }
        else
        {
            Destroy(gameObject); // destory game object on scene swtich or scene restart
        }


        if (SeedType == RandomOrDay.Random) // random type
		{
			SeedMapBool = UnityEngine.Random.Range(0, 1000); // random number range 0 to 1000

		}
		if (SeedType == RandomOrDay.Day) // random type day
		{
			//PlayerPrefs.DeleteAll();
			Date = System.DateTime.UtcNow.Day.ToString(); // sets current day to a string

			if (PlayerPrefs.GetString("OldDate") != "") // old date is not the same string
			{
				Debug.Log("Yes");
				OldDate = PlayerPrefs.GetString("OldDate"); // save old date to player prefs
				SeedMapBool = PlayerPrefs.GetInt("RandomNumber"); // save old random seed to player prefs

				if (Date != OldDate) // date does not equal old date
				{
					OldDate = Date; // set old date to date
					PlayerPrefs.SetString("OldDate", OldDate); // saves old date to player prefs
					SeedMapBool = UnityEngine.Random.Range(0, 1000); // seedbool set to random number
					PlayerPrefs.SetInt("RandomNumber", SeedMapBool); // save player prefs of the random bumber and the seed map bool
				} 
			}
			else
			{
				Debug.Log("No");
				OldDate = Date; // old date set to date
				PlayerPrefs.SetString("OldDate", OldDate); // set string to old date
				SeedMapBool = UnityEngine.Random.Range(0, 1000); // seed mapbool to random range
				PlayerPrefs.SetInt("RandomNumber", SeedMapBool); // save random number seedmap bool
			}
		}

        if (PlayerPrefs.GetInt("PlayerAmount") == 0) //one player or two
        {
            twoPlayers = false; // start out one player
        }
        else
        {
            twoPlayers = true; // start out one player
        }
    }

    public void SpawnPlayer()
    {
        Debug.Log("Run");
        spawn = UnityEngine.Random.Range(0, respawn.Length); // random spawn point for single player

        player.transform.position = respawn[spawn].transform.position; // position to random spawn
        player.transform.rotation = respawn[spawn].transform.rotation; // roation for radonm spawn
        player.gameObject.SetActive(true);

        if (twoPlayers) // two players
        {
            spawnTwo = UnityEngine.Random.Range(0, respawn.Length); // randomw spawn point

            playerTwo.transform.position = respawn[spawnTwo].transform.position; // random spawn point for second player
            playerTwo.transform.rotation = respawn[spawnTwo].transform.rotation; // random rotation for second player
            playerTwo.gameObject.SetActive(true); // creates teh second player
        }
    }
}
	


