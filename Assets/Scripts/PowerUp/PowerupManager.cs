﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour
{
    public List<PowerupType> powerup; // power up type

    private TankData data; // pulls from tank data

    // Use this for initialization
    void Start()
    {
        data = GetComponent<TankData>(); // get tank data
    }

    // Update is called once per frame
    void Update()
    {
        List<PowerupType> powerupsToRemove = new List<PowerupType>(); // remove power up from list

        foreach (PowerupType powerup in powerup)
        {
            powerup.CountDown -= Time.deltaTime; // count down until power up is no longer used

            if (powerup.CountDown <= 0) // if it's less or equal to 0
            {
                powerupsToRemove.Add(powerup); // remove power up from add
            }
        }
        foreach (PowerupType powerup in powerupsToRemove)
        {
            RemovePowerup(powerup); // remove power up
        }
    }

    public void AddPowerup(PowerupType powerupToAdd)
    {
        PowerupType newPowerup = new PowerupType(powerupToAdd); // power up to add
        powerup.Add(newPowerup); // add power up
        powerupToAdd.OnAddPowerup(data); // add power up
    }

    public void RemovePowerup(PowerupType powerupToRemove)
    {
        powerup.Remove(powerupToRemove); // remove power up
        powerupToRemove.OnRemovePowerup(data); // remove power up
    }
}
