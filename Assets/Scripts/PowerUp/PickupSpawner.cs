﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour
{
    public GameObject[] PickupPrefab; // prefab

    public Transform[] SpawnPoint; // spawn points for prefabs
    public float SpawnDelay; // spawn dwlay for points

    private GameObject SpawnedPickup; // spawned pick up 
    private float NextSpawnTime; // time until next spawn

    private int RanSpawn; // random spawn
    private int RandomSpawn; // random spawn

    // Use this for initialization
    void Start()
    {
        NextSpawnTime = Time.time + SpawnDelay; // time to next spawn can be set by designer
    }

    // Update is called once per frame
    void Update()
    {
        RandomSpawner(); // runs random spawner every frame
    }

    void RandomSpawner()
    {
        RanSpawn = Random.Range(0, SpawnPoint.Length); // randomly chooses from the spawn point list
        RandomSpawn = Random.Range(0, PickupPrefab.Length); // randomly chooses a prefab

        if (SpawnedPickup == null)
        {
            if (Time.time > NextSpawnTime) // once timer runs out
            {
                SpawnedPickup = Instantiate(PickupPrefab[RandomSpawn], SpawnPoint[RanSpawn].position, Quaternion.identity) as GameObject; // creates the prefab and gives it the location
                NextSpawnTime = Time.time + SpawnDelay; // time to next spawn
            }
        }
        else
        {
            NextSpawnTime = Time.time + SpawnDelay; // time to next spawn
        }
    }    
}
