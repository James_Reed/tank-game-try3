﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupDamage : MonoBehaviour
{
    public PowerupType powerUps; // power up

    public enum PowerType { Damage, Health }; // type
    public PowerType PowerTypes; // listed type

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.isPickup == true) // is it a pick up
        {
            
            GameManager.instance.isPickup = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<TankData>()) // pulls from tank data
        {
            GameManager.instance.isPickup = true; // is it a pickup

            PowerupManager pm = other.GetComponent<PowerupManager>(); // pulls from power up manager

            if (pm != null)
            {
                pm.AddPowerup(powerUps); // add power up
                Destroy(gameObject); // destory object after it's picked up
            }
        }
    }   // Start is called before the first frame update
  
}
