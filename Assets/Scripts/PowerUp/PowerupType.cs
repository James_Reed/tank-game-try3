﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupType : MonoBehaviour
{
    public enum PowerType { Damage, Health }; // enum for damage or health
    public PowerType PowerTypes; // types

    public float BonusAmount; // bonus amount of damage can be set by designer
    public float CountDown; // count down can be set by designer

    public bool IsPerm; // is it perminanat or not Boolean that can be changed on the prefab 

    public PowerupType() // Need for power ups to work
    {

    }

    public PowerupType(PowerupType powerupToClone) //  need for power ups to work
    {

    }

    public void OnAddPowerup(TankData data) // add the power up to the correct tank data stat
    {
        if (PowerTypes == PowerType.Health) // is it health
            data.Health += BonusAmount; // add to health bonus amount can be set by designers
        if (PowerTypes == PowerType.Damage) // is it damage
            data.Damage += BonusAmount; // add to base damage amount can be set by designers
    }

    public void OnRemovePowerup(TankData data) // removing the power up
    {
        if (IsPerm) // is it perm
        {
            return; // returns
        }

        data.Health -= BonusAmount; // take away bonus amount of health
        data.Damage -= BonusAmount; // removes bonus damage
    } 

}
