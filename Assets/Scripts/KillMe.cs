﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillMe : MonoBehaviour
{
    public float CountDown = 30; // variable for counting down
    public float Damage; // damage the bullet will do
    // Start is called before the first frame update
    void Start()
    {
       
    }
     
   private void OnTriggerEnter(Collider other) // when a trigger is hit
    {
        if (other.gameObject.GetComponent<TankData>()) // pull from tank data
        {
            TankData hc = other.gameObject.GetComponent<TankData>(); // pull health from tank data
            Debug.Log(hc); // make sure the data is pulled
            hc.RemovingHealth(Damage); // remove the damage from the health
        }
    }

    // Update is called once per frame
    void Update()
    {
        CountDown -= Time.deltaTime; // countdonw is subtracted by time.deltatime
        if (CountDown <= 0) // once it hits zero, runs
        {
            Destroy(gameObject); // destory the game object this script is attached to
        }

    }
}
