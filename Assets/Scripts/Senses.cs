﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Senses : MonoBehaviour
{
    public AIController playerseeker; // seek out player

    // Start is called before the first frame update
    void Start()
    {
        playerseeker = GetComponentInParent<AIController>(); // pull the controller
    }

    void Update()
    {
        if (playerseeker.target != null)
        {
            playerseeker.SeekPoint(playerseeker.target.transform.position); // seek the next point
        }
    }
    //Update is called once per frame
    void OnTriggerEnter(Collider enter) // when a trigger htis
    {
        Debug.Log("Hit");
        if (enter.gameObject.tag == "Noise") // check tag 
        {
            Debug.Log("Hit");
            playerseeker.target = enter.gameObject; // sets target to gameobject touched.
        }
    }

    

}
