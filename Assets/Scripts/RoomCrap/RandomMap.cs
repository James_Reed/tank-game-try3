﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMap : MonoBehaviour
{
	public List<RoomHolder> rooms; // rooms list

	//enemie stuff
	public int typeOfEnemy; // type of enemy 
	public GameObject[] enemyTankPrefab; // prefabs
	public int amountOfEnemies; // amount of enmies
	public int maxNumOfEnemies; // max enemies
	public int enemySpawn; // spawn enemies
	public int enemyTank; // enemy tank
    public EnemyRespawn[] enemyRespawn; // respawn enemy location
    


    public float TileXWidth; // width for room spawn
	public float TileZWidth; // width for room spawn

	public int NumberOfColumns; // number of columns

	public int NumberOfRows; // number of rows
	public int SeedMap; // seed number

	public List<GameObject> RoomPrefabs; // list of room prefabs

	private RoomHolder[,] World; // world naming

    // Start is called before the first frame update
    void Awake()
    {
		MapGeneration(); // function
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void MapGeneration()
	{

		Random.InitState(GameManager.instance.SeedMapBool); // create the rrom
		World = new RoomHolder[NumberOfColumns, NumberOfRows]; // rows and colums

		for (int CurrentRow = 0; CurrentRow < NumberOfRows; CurrentRow++)
		{
			for (int CurrentColumn = 0; CurrentColumn < NumberOfColumns; CurrentColumn++)
			{
				int Rand = Random.Range(0, RoomPrefabs.Count); // random range
				GameObject NewRoom = Instantiate(RoomPrefabs[Rand]) as GameObject; // new room pulled
                RoomHolder roomHolder = NewRoom.gameObject.GetComponent<RoomHolder>(); // room holder
                rooms.Add(roomHolder);

                NewRoom.name = "Room (" + CurrentColumn + "," + CurrentRow + ")"; // naming for inspector
				NewRoom.transform.parent = this.transform; // room transform

				float XPosition = TileXWidth * CurrentColumn; // new position
				float ZPosition = TileZWidth * CurrentRow; // new position
				Debug.Log(XPosition);
				Debug.Log(ZPosition);

				NewRoom.transform.localPosition = new Vector3(XPosition, 0f, ZPosition); // new postions x and y

				RoomHolder TempRoom = NewRoom.GetComponent<RoomHolder>(); // put room into the list

				if (CurrentRow == 0)
				{
					TempRoom.Room3.SetActive(false); 
				}
				if (CurrentRow == 1)
				{
					TempRoom.Room3.SetActive(false);
					TempRoom.Room4.SetActive(false);
				}
				if (CurrentRow == 2)
				{
					TempRoom.Room4.SetActive(false);
				}
				if (CurrentColumn == 0)
				{
					TempRoom.Room2.SetActive(false);
				}
				if (CurrentColumn == 1)
				{
					TempRoom.Room2.SetActive(false);
					TempRoom.Room1.SetActive(false);
				}
				if (CurrentColumn == 2)
				{
					TempRoom.Room1.SetActive(false);
				}
				World[CurrentColumn, CurrentRow] = TempRoom;
			}
		}
        SpawnEnemy(); // spawn enemy after the world is created
        GameManager.instance.respawn = FindObjectsOfType<Respawn>(); // find respawn locations
        GameManager.instance.SpawnPlayer();// creates the player
    }

	public GameObject RandomRoomPrefab()
	{
		return RoomPrefabs[Random.Range(0, RoomPrefabs.Count)]; // room prefab random range
	}
    // Enemy spawn stuff
   
	void SpawnEnemy()
    {
        enemyRespawn = FindObjectsOfType<EnemyRespawn>(); // find object enemy respawn

        for (int i = 0; i < maxNumOfEnemies; i++)
        {

            typeOfEnemy = Random.Range(0, enemyTankPrefab.Length); // list of enemys types
            enemySpawn = Random.Range(0, enemyRespawn.Length); // list of enemy spawn spots
            int ranRoom = Random.Range(0, rooms.Capacity); // room capacity
            Debug.Log(ranRoom);

            GameObject AiTank = Instantiate(enemyTankPrefab[typeOfEnemy], enemyRespawn[enemySpawn].transform.position, enemyRespawn[enemySpawn].transform.rotation) as GameObject; // create take along with location and rotation
            PatrolAI CurrentWayPoint = AiTank.GetComponent<PatrolAI>(); //waypoint for the AI
        }
           
	}

  
}
