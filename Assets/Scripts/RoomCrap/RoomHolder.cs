﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomHolder : MonoBehaviour
{
	public GameObject Room1; // holds room prefab
	public GameObject Room2;
	public GameObject Room3;
	public GameObject Room4;

    public List<Transform> Waypoints; // holds waypoints
}
